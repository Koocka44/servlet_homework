package com.epam.training.forum.tlib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class ForeachTagService extends SimpleTagSupport {

	private List<Object> source;
	private String var;
	private String statusVar;

	public void setVar(String var) {
		this.var = var;
	}

	@SuppressWarnings("unchecked")
	public void setSource(Object source) {
		this.source = null;
		if (source instanceof Collection) {
			this.source = new ArrayList<Object>((Collection<Object>) source);
		} else {
			this.source = Arrays.asList((Object[]) source);
		}
	}

	public void setStatusVar(String statusVar) {
		this.statusVar = statusVar;
	}

	public void doTag() throws JspException, IOException {

		JspContext context = getJspContext();
		Status status = new Status();
		status.setCollectionSize(source.size());
		int index = 1;
		for (Object object : source) {
			status.setIndex(index);
			context.setAttribute(var, object);
			context.setAttribute(statusVar, status);
			getJspBody().invoke(null);
			index++;
		}
		context.removeAttribute(var);
		context.removeAttribute(statusVar);
	}

}