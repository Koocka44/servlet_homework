package com.epam.training.forum.model;

public class Comment {

	private String creationDate;
	private String text;
	private String userName;

	public Comment(String userName, String text, String creationDate) {
		this.creationDate = creationDate;
		this.text = text;
		this.userName = userName;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
