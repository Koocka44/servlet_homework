package com.epam.training.forum.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Database {

	private Map<User, String> identifiers = new HashMap<User, String>();
	private Set<User> users = new HashSet<User>();
	private List<Comment> comments = new ArrayList<Comment>();
	private static Database instance;

	private Database() {

	}

	public static synchronized Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}

	public boolean addUser(User user) {
		return users.add(user);
	}

	public List<Comment> getComments() {
		return this.comments;
	}

	public void addComment(Comment comment) {
		comments.add(comment);
	}

	public void removeIdentifierOfUser(User user) {
		identifiers.remove(user);
	}

	public void addIdentifier(User user, String identifier) {
		identifiers.put(user, identifier);
	}

	public User findUserForIdentifier(String rememberMeCode) {
		User user = null;
		for (Entry<User, String> entry : identifiers.entrySet()) {
			if (entry.getValue().equals(rememberMeCode)) {
				user = entry.getKey();
			}
		}
		return user;
	}

	public User findUserForCredentials(String username, String password) {
		User user = null;
		for (User currentUser : users) {
			if (currentUser.getUserName().equals(username) && currentUser.getPassword().equals(password)) {
				user = currentUser;
			}
		}
		return user;
	}
}
