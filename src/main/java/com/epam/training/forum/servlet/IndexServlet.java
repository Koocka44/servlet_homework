package com.epam.training.forum.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.training.forum.model.Comment;
import com.epam.training.forum.model.Database;
import com.epam.training.forum.model.User;

public class IndexServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Database db = Database.getInstance();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = null;
		synchronized (session) {
			user = (User) session.getAttribute("user");
		}
		if (user == null) {
			response.sendRedirect("login");
		} else {
			List<Comment> comments = new ArrayList<Comment>();
			synchronized (db) {
				comments = db.getComments();
			}
			request.setCharacterEncoding("UTF-8");
			request.setAttribute("comments", comments);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = null;
		synchronized (session) {
			user = (User) session.getAttribute("user");
		}
		if (user == null) {
			response.sendRedirect("login");
		}
		request.setCharacterEncoding("UTF-8");
		String text = request.getParameter("text");
		if (!text.isEmpty()) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
			String creationDate = formatter.format(new Date());
			synchronized (db) {
				db.addComment(new Comment(user.getUserName(), text, creationDate));
			}
		}
		response.sendRedirect("index");
	}
}
