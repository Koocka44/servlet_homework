package com.epam.training.forum.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.training.forum.model.Database;
import com.epam.training.forum.model.User;

public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Database db = Database.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		synchronized (session) {
			if (session.getAttribute("user") == null) {
				if (session.getAttribute("message") != null) {
					request.setAttribute("message", session.getAttribute("message"));
					session.removeAttribute("message");
				}
				request.getRequestDispatcher("/registrationForm.jsp").forward(request, response);
			} else {
				response.sendRedirect("index");
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		boolean success;
		if ("".equals(userName) || "".equals(password)) {
			request.setAttribute("message", "Username and password cannot be empty!");
			request.getRequestDispatcher("/registrationForm.jsp").forward(request, response);
		} else {
			synchronized (db) {
				success = db.addUser(new User(userName, password));
			}
			if (success) {
				synchronized (session) {
					session.setAttribute("message", "Registration successful!");
				}
				response.sendRedirect("login");
			} else {
				request.setAttribute("message", "User already exists!");
				request.getRequestDispatcher("/registrationForm.jsp").forward(request, response);
			}
		}
	}

}
