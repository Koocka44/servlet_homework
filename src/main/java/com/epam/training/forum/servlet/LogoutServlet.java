package com.epam.training.forum.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.training.forum.model.Database;
import com.epam.training.forum.model.User;

public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Database db = Database.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = null;
		synchronized (session) {
			user = (User) session.getAttribute("user");
		}
		if (user != null) {
			synchronized (db) {
				db.removeIdentifierOfUser(user);
			}
			synchronized (session) {
				session.invalidate();
			}
		}
		response.sendRedirect("login");
	}
}
