package com.epam.training.forum.servlet;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.training.forum.model.Database;
import com.epam.training.forum.model.User;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Database db = Database.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		synchronized (session) {
			if (session.getAttribute("user") == null) {
				if (session.getAttribute("message") != null) {
					request.setAttribute("message", session.getAttribute("message"));
					session.removeAttribute("message");
				}
				Cookie[] cookies = request.getCookies();
				Cookie remembermeCookie = null;
				if (cookies != null) {
					for (int i = 0; i < cookies.length; i++) {
						if (cookies[i].getName().equals("rememberme")) {
							remembermeCookie = cookies[i];
						}
					}
					if (remembermeCookie != null) {
						String rememberMeCode = URLDecoder.decode(remembermeCookie.getValue(), "UTF-8");
						synchronized (db) {
							User user = db.findUserForIdentifier(rememberMeCode);
							if (user != null) {
								session.setAttribute("user", user);
								response.sendRedirect("index");
								return;
							}
						}
					}
				}
				request.getRequestDispatcher("/loginForm.jsp").forward(request, response);
			} else {
				response.sendRedirect("index");
			}
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String rememberme = request.getParameter("rememberme");
		User user = null;
		synchronized (db) {
			user = db.findUserForCredentials(username, password);
		}
		if (user != null) {
			if ("rememberme".equals(rememberme)) {
				String remembermeIdentifier = username + password;
				Cookie remembermeCookie = new Cookie("rememberme", URLEncoder.encode(remembermeIdentifier, "UTF-8"));
				remembermeCookie.setMaxAge(30 * 60);
				response.addCookie(remembermeCookie);
				db.addIdentifier(user, remembermeIdentifier);
			}
			synchronized (session) {
				session.setAttribute("user", user);
			}
			response.sendRedirect("index");
		} else {
			request.setAttribute("message", "Invalid username or password!");
			request.getRequestDispatcher("/loginForm.jsp").forward(request, response);
		}
	}

}
