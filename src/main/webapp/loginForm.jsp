<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Forum Login</title>
</head>
<body>
	<form action="login" method="post">
		Username <input type="text" name="username"><br>
		Password <input type="password" name="password"><br>
		Remember me! <input type="checkbox" value="rememberme" name="rememberme"><br>
		${message}<br>
		<input type="submit" value="login"><br>
		<a href="registration">I want to sign up!</a>
	</form>
</body>
</html>