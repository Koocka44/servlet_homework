<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mine" uri="OwnTagLib"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Forum</title>
<link rel="stylesheet" href="style.css"/>
</head>
<body>
	<a href="logout">Get me outta here!!!!!</a><br>
	<c:if test="${fn:length(comments) eq 0}">
		There are no comments yet!
	</c:if>
	<c:if test="${fn:length(comments) gt 0}">
		<c:if test="${fn:length(comments) gt 1}">
			There are ${fn:length(comments)} comments:<br>
		</c:if>
		<c:if test="${fn:length(comments) lt 2}">
			There is 1 comment:<br>
		</c:if>
		<table>
			<mine:forEach var="currentComment" source='${comments}'
				statusVar="status">
				<tr>
					<td class="firstCell">
					<span class="userName">${currentComment.userName }</span><br>
					<span class="commentDate">at: ${currentComment.creationDate}</span>
					</td>
					<td class="secondCell">${currentComment.text }</td>
				</tr>
			</mine:forEach>
		</table>
	</c:if>
	<form action="index" method="post">
		Your comment here: <br>
		<textarea name="text"></textarea>
		<input type="submit" value="Submit">
	</form>
</body>
</html>