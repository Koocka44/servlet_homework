<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Forum Login</title>
</head>
<body>
	<form action="registration" method="post">
		Username <input type="text" name="username"><br>
		Password <input type="password" name="password"><br>
		${message}<br>
		<input type="submit" value="Sign Up"><br>
		<a href="login">I want to log in!</a>
	</form>
</body>
</html>